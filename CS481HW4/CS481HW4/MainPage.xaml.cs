﻿using System;
using CS481HW4;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using Xamarin.Forms;

 namespace CS481HW4
{
    public partial class MainPage : ContentPage
    {
        ObservableCollection<artist> information = new ObservableCollection<artist>();
        public MainPage()
        {
            InitializeComponent();
            PopulateListView();
        }

        private void ViewCells_ItemTapped(object sender, ItemTappedEventArgs e)
        {

        }
        private void PopulateListView()
        {
            var artist1 = new artist();
            artist1.name = "Kota The Friend";
            artist1.genre = "Rap";
            artist1.bestSong = "Top Song: Smile";
            artist1.Image = "kota.png";

            information.Add(artist1);

            var artist2 = new artist();
            artist2.name = "Rise Against";
            artist2.genre = "Genre:Punk Rock";
            artist2.bestSong = "Top Song: Give It All";
            artist2.Image = "riseAgainst.png";
            information.Add(artist2);

            var artist3 = new artist();
            artist3.name = "Dance Gavin Dance";
            artist3.genre = "Genre:Rock";
            artist3.bestSong = "Top Song: Me and Zoloft Get Along Just Fine";
            artist3.Image = "dgd.png";
            information.Add(artist3);

            var artist4 = new artist();
            artist4.name = "Weezer";
            artist4.genre = "Genre:Rock";
            artist4.bestSong = "Top Song: Memories";
            artist4.Image = "weezer.png";
            information.Add(artist4);

            var artist5 = new artist();
            artist5.name = "Khai Dreams";
            artist5.genre = "Genre:Rap";
            artist5.bestSong = "Top Song: All I need";
            artist5.Image = "khai.png";
            information.Add(artist5);

            ViewCells.ItemsSource = information;

        }

        private void ViewCells_Refreshing(object sender, EventArgs e)
        {
            PopulateListView();

            ViewCells.IsRefreshing = false;

        }

        public void MenuItem_Clicked(object sender, EventArgs e)
        {
            var deleteAction = new MenuItem { Text = "Delete", IsDestructive = true };
            var deleteInfo = ((MenuItem)sender);

            information.Remove((CS481HW4.artist)deleteInfo.CommandParameter);

        }
    }
}

